;;;;;
title: CL Docker Images
format: md
URL: index.html
;;;;;

# Overview

The purpose of this project is to make it easy to use any open source ANSI
Common Lisp implementation inside a Docker container. To that end, we publish a
variety of prebuilt images to
the [`clfoundation` org on Docker Hub](https://hub.docker.com/u/clfoundation)
that share common layers and methodologies (such as entrypoint behavior) as
much as possible.

Some people love it, some people hate it, but Docker is a useful technology for
certain CI/CD, deployment, and development scenarios. In addition to the images
themselves, we plan on show casing [uses of these images](use-cases.html).

We publish the following types of images:

-   **[Implementation specific](images/implementation-specific.html):** These
    images contain a single Common Lisp implementation.
-   **[Development](images/development.html):** These images are geared toward
    interactive development.

We publish images for the following platforms. If someone would like to
donate hardware or expertise for setting up environments for other platforms,
please reach out!

-   `linux/amd64`
-   `linux/arm/v7`
-   `linux/arm64/v8`
-   `windows/amd64`

Additionally, the
[`slime-docker`](https://gitlab.common-lisp.net/cl-docker-images/slime-docker)
Emacs package is part of this project as well. Se eits README for more
information on how to integrate Docker images into your SLIME workflow.

The following pages contain more detail on this project:

- **[Roadmap](roadmap.html):** Where this project is going in the future.
- **[Infrastructure](infrastructure.html):** Where the code is hosted and how
  the images are built.
- **[History](history.html):** Where these images came from.
- **[Contributing](contributing.html):** How to contribute to this project.
- **[Code of Conduct](code-of-conduct.html):** How we expect contributors to
  interact with each other.
